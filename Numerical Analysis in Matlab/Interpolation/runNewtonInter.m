function [func, coeffs, dividedDiff,time] = runNewtonInter (x,y,p,n)
tic;
 [func, coeffs, dividedDiff]= newtonInterpol(x,y,p,n);
 f=inline(func);
time=toc;

hold on;
t = linspace(x(1) , x(length(x)));
t2 = f(t) ;
plot(t , t2);
plot(p , f(p),'or');
plot(x,f(x),'og');
title('Newton Interpolation');
legend('Function', 'Interpolation','Samples');
hold off; %/--------
end
