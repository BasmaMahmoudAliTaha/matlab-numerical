function [output, func , time ]= lagrange( x , y , q , order) 
tic ;
hold off 

acc = '' ;
for  i = 0: order
       
   s = num2str(y(i+1)) ;
   li = L(x , i , order)  ; 
   
   acc = [acc , ' + ' , s , ' .* ' , li ] 
end 
   
func = acc ;
f2 = inline(acc);
output = f2(q) ; 
disp('------------------*-*-*- ---');
disp(output);
disp('------------------*-*-*- ---');
hold on;

temp1 = linspace(x(1) , x(order+1)) ; 
temp2 = f2(temp1) ;
plot(temp1 , temp2 ) ;
plot(q , output , 'or');
plot(x , y , 'og') ;
hold off;

time = toc

end