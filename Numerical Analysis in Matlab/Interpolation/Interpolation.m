function varargout = Interpolation(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Interpolation_OpeningFcn, ...
                   'gui_OutputFcn',  @Interpolation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Interpolation is made visible.
function Interpolation_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for Interpolation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(findall(handles.inPanel, '-property', 'enable'), 'enable', 'off')

% UIWAIT makes Interpolation wait for user response (see UIRESUME)
% uiwait(handles.introPanel);


% --- Outputs from this function are returned to the command line.
function varargout = Interpolation_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


% --- Executes on button press in netwonBut.
function netwonBut_Callback(hObject, eventdata, handles)
set(handles.netwonBut,'SelectionHighlight','on');
set(handles.lagrange,'SelectionHighlight','off');
set(handles.type,'String','Newton Method');

set(handles.pointPanel,'Visible','on');
set(handles.outPanel,'Visible','off');
cla(handles.axes1);
set(findall(handles.inPanel, '-property', 'enable'), 'enable', 'off')

% --- Executes on button press in lagrange.
function lagrange_Callback(hObject, eventdata, handles)
set(handles.netwonBut,'SelectionHighlight','off');
set(handles.lagrange,'SelectionHighlight','on');

cla(handles.axes1)
set(handles.type,'String','Lagrange Method');
set(handles.pointPanel,'Visible','on');
set(handles.outPanel,'Visible','off');
set(findall(handles.inPanel, '-property', 'enable'), 'enable', 'off')


function numbOfPoints_Callback(hObject, eventdata, handles)



% --- Executes during object creation, after setting all properties.
function numbOfPoints_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
numberOfPoints=0;

% --- Executes on button press in done1.
function done1_Callback(hObject, eventdata, handles)
x= get(handles.sPointsTxt,'String');
y= get(handles.qPointsTxt,'String');

if isempty(x) || isempty(y)
msgbox('please enter the number of sample points and query points','warning');
else

tab = cell(str2num(x),2);
 set(handles.table1, 'Data', tab);

set(handles.inPanel,'Visible','on');
set(handles.tempPanel,'Visible','on');
abc2= cell(str2num(y),1);
set(handles.table2,'Data',abc2);
set(handles.table3,'Data',abc2);
set(handles.pointPanel,'Visible','off');
set(findall(handles.inPanel, '-property', 'enable'), 'enable', 'on')
end 
 
%------------------------------------------------------------------------------solving---------------------------------------------------------------------
% --- Executes on button press in done2.
function done2_Callback(hObject, eventdata, handles)
 flag=true;
dataPoints= get(handles.table1,'Data');

h= length(dataPoints);
  for z=1:h
         singlePoint=dataPoints{z,1};
          pointRes=dataPoints{z,2};
         if isempty(singlePoint) || isempty(pointRes)
             flag=false;
             break;
         end
    X(z)=singlePoint;
    F(z)=pointRes;
  end
 if flag==true
      queryPoints= get(handles.table2,'Data');
      h2= length(queryPoints);
      for k=1:h2
         qPoint=queryPoints{k,1};
         disp(qPoint);
  if isempty(qPoint)
        break;
  end
  if isstr(qPoint)
      qPoint=str2double(qPoint)
  end
  
    queryPointsArray(k) =(qPoint);
      end 
  
         if  strcmp(get(handles.netwonBut,'SelectionHighlight'),'on') ==1  %------------------------------------------------------------Newton---------------
         [ interpolatedFunc, coeffs, dividedDiff,time ]= runNewtonInter(X,F,queryPointsArray,h);
 disp(' divided difference');
   disp(dividedDiff);
     tableee=cell(length(coeffs),1);
       for p =1:length(coeffs)   
         tableee(p,1)=num2cell(coeffs(p));
     end 
     set(handles.table3,'Data',tableee);
     set(handles.funcText,'String',interpolatedFunc);
     set(handles.txtTime,'string',time);
     set(handles.outPanel,'Visible','on');
     
     elseif strcmp(get(handles.lagrange,'SelectionHighlight'),'on') ==1 %----------------------------------------------------------------lagrange---------------

   [ output, interpolatedFunc,time]= lagrange(X,F,queryPointsArray,h-1);
   disp(output);
   disp(interpolatedFunc);
     tableee=cell(length(output),1);
       for p =1:length(output)   
         tableee(p,1)=num2cell(output(p));
     end 
   set(handles.table3,'Data',tableee);
      set(handles.funcText,'String',interpolatedFunc);
     set(handles.txtTime,'string',time);
        set(handles.outPanel,'Visible','on');
     else
          msgbox('choose way of solving ','warning');
     end
 else
     msgbox('please enter all the fields ','warning');
 end






% --- Executes on button press in fileCh.
function chFile_Callback(hObject, eventdata, handles)

[FileName,PathName] = uigetfile('*.txt','Select input file');
fullPath=strcat(PathName,FileName);
line =fileread(fullPath);
op=regexp(line, '[ \s+]', 'split')
cellss=cell(length(op),1);
for i=1:length(op)
    isstr(op(i));
    cellss(i,1)=cellstr(op(i))
end
set(handles.table2,'Data',cellss);



function numOfQuery2_Callback(hObject, eventdata, handles)
% hObject    handle to numOfQuery2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numOfQuery2 as text
%        str2double(get(hObject,'String')) returns contents of numOfQuery2 as a double


% --- Executes during object creation, after setting all properties.
function numOfQuery2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numOfQuery2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes during object creation, after setting all properties.
function sPointsTxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sPointsTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function qPointsTxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to qPointsTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end







function sPointsTxt_Callback(hObject, eventdata, handles)
% hObject    handle to sPointsTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sPointsTxt as text
%        str2double(get(hObject,'String')) returns contents of sPointsTxt as a double



function qPointsTxt_Callback(hObject, eventdata, handles)
% hObject    handle to qPointsTxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of qPointsTxt as text
%        str2double(get(hObject,'String')) returns contents of qPointsTxt as a double


% --- Executes during object creation, after setting all properties.
function funcText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to funcText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --------------------------------------------------------------------
function table1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to table1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
