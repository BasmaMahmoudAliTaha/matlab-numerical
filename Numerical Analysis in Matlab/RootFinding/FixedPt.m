function [xr,y,validEntry,warning,gOfX,itTable,time]=FixedPt(func, x0, es, iter_max)
warning='no error';
time=0;
validEntry=1;
tic
hold off
itTable =cell( iter_max,3);
xr(1) = x0;
y(1) = feval(inline(func),xr(1));
ea(1)=1/0;

%itTable(1,3)=num2cell(ea(1));
%itTable(1,1)=num2cell(xr(1));
%itTable(1,2)=num2cell(y(1));

double xr_old;
iter=2;
%temp = @(x) x;
display(isstr(func));
g= [func,' + x']
gOfX=g;

while(iter <= iter_max+1)
    xr_old = xr(iter-1);
    xr(iter) = feval(inline(g),xr_old);
    itTable(iter-1,1)=num2cell(xr(iter-1));
    itTable(iter-1,2)=num2cell(xr(iter));
    
    %-*------------------------------------------
    fig=figure;
    hax=axes;
    syms x
    A = sym(g);
    b = matlabFunction(A);
    x = (x0-2*x0-2):.1:(x0+2*x0+2);
    plot(x,b(x))
    title('Graph of x=g(x) and y=x for fixed point method')
    hold on ;
    %--------------
    plot(x,x,'Color',[.6 0 0]);
    
    %--------------------
    hold on ;
    sp1 = xr(iter);
    y1=get(gca,'ylim');
    line([sp1 sp1],y1, 'color',rand(1,3))
    %legend('x=g(x)','y=x','x=xr')
    legend(['f1(x)=' num2str(g)],['f2(x)=x'],['x=' num2str(xr(iter))])
    pause
    close(fig);
    %-*------------------------------------------
    
    if (xr(iter) == 0)
        warning='real root is reached';
        break;
    end
    ea(iter) = abs((xr(iter) - xr_old) / xr(iter));
    itTable(iter-1,3)=num2cell(ea(iter));
    iter=iter+1;
    if(ea(iter-1) < es  )
        warning='reached desired value of tolerance , fixed point method has converged';
        break;
    end
end

if(ea(iter-1) > es  )
    warning='Maximum iterations reached and desired tolerance isn''t reached yet';
    
end


n=length(xr);k=1:n;out=[k;xr(1:n);ea(1:n)];
disp('Iterations     xr        es');
fprintf('%d              %f      %f\n',out);

toc;
time=toc;
%if (feval(inline(func),xr(iter-1))>2)
%    warning='The method diverged';
%    disp('The method diverged');
%end

end
