function  [validEntry,notificationString,itTable,time]=false_position(func,xl,xu,es,maxit)
time=0;
tic
notificationString='no error';
 validEntry=1;
itTable =cell(maxit,8);

l(1) = xl;
U(1) = xu;
ya(1)= feval(inline(func),xl);
yb(1) = feval(inline(func),xu);

disp (ya(1));
disp (yb(1));

ea(1)=1/0;
 itTable(1,1)=num2cell(xl);
 itTable(1,2)=num2cell(ya(1));
 itTable(1,3)=num2cell(xu);
 itTable(1,4)=num2cell(yb(1));
disp (l(1));

if ya(1) * yb(1) > 0.0
    validEntry=0;
    notificationString='Function has same sign';
    return;
else
    for i=1:maxit
          Sol(i)=(l(i)*yb(i)-U(i)*ya(i))/(yb(i)-ya(i));
          y(i) =feval(inline(func),Sol(i));
          itTable(i,1)=num2cell(l(i));
          itTable(i,3)=num2cell(U(i));
                    itTable(i,2)=num2cell(ya(i));
          itTable(i,4)=num2cell(yb(i));
         itTable(i,5)=num2cell(Sol(i));
        itTable(i,6)=num2cell(y(i));
        
        %-*------------------------------------------
    fig=figure;
    hax=axes;
    syms x
    A = sym(func);
    b = matlabFunction(A);
    x = xl-5:.1:xu+5;
    plot(x,b(x))
    hold on ;    
    sp1 = l(i);
    sp2 = U(i);
    y1=get(gca,'ylim');
    line([sp1 sp1],y1, 'color',rand(1,3))
    line([sp2 sp2],y1, 'color',rand(1,3))
    title('Graph of y=f(x) for false position method')
    legend(['y=' num2str(func)],['x_l=' num2str(l(i))],['x_u=' num2str(U(i))])
    pause 
    close(fig)
    %-*------------------------------------------
        if y(i) == 0.0
              notificationString='exact zero found';
            break;
        elseif y(i)*yb(i) <0
            l(i+1)=Sol(i);
            ya(i+1)=y(i);
            U(i+1)=U(i);
            yb(i+1)=yb(i);

        else
            l(i+1)=l(i);
            ya(i+1)=ya(i);
            U(i+1)=Sol(i);
            yb(i+1)=y(i);
        end;
        
        if (i>1) 
            ea(i)=abs(Sol(i) - Sol(i-1))/Sol(i);
              itTable(i,7)=num2cell(ea(i));
            if (ea(i) <es)
                notificationString='desired tolerance is reached , False position method has converged ';
               break;
            end   
        end
        iter=i;
    end
    
    if(ea(i)>es)
        notificationString='zero not found to desired tolerance';
    end
    n=length(Sol);k=1:n;out=[k;l(1:n);U(1:n);ea(1:n);Sol;y];
    disp   ('itertions      xl            xu            es            xr            f(xr)');
    fprintf('%d              %f      %f     %f      %f      %E\n',out);
end;
time=toc
end


