function[validEntry,notificationString, table,time]=secant(func,z,y,eps,maxit)  % latest version
table =cell(maxit,6);
time=0;
tic
notificationString='no errors';
validEntry=1;
%-------------------------------------
  fig=figure;
syms x
A = sym(func);
b = matlabFunction(A);
x = -10:.1:10;
plot(x,b(x))
hold on
%-------------------------------------
sol(1)=z;
sol(2)= y ;
f(1)=feval(inline(func),sol(1));
f(2)=feval(inline(func),sol(2));

disp (' Step         xi                        fi                        xi-1                        fi-1');
for i=3 :maxit
    table(i-2,1)=num2cell(sol(i-1));
    table(i-2,2)=num2cell(f(i-1));
    sol(i)= sol(i-1)-f(i-1)*(sol(i-2)-sol(i-1))/(f(i-2)-f(i-1));
    table(i-2,3)=num2cell(sol(i));
    f(i)=feval(inline(func),sol(i));
    table(i-2,4)=num2cell(f(i));
    out = [i-2 ;sol(i) ; f(i);sol(i-1) ; f(i-1)];
    %-*------------------------------------------
    slope = (f(i-1)-f(i-2))/(sol (i-1)-sol(i-2))
    tang=(x-sol(i-1))*slope+f(i-1);
    plot(x,tang, 'Color', rand(1,3))
    scatter(sol(i-1),f(i-1))
    scatter(sol(i-2),f(i-2))
    hold on
   
   fprintf ('%5.0f     %20.14f     %21.15f     %21.15f\n',out);
   fprintf('\n');
   fprintf ('ERROR = %21.15f\n',abs (sol(i) -sol(i-1)));
   table(i-2,6)=num2cell(abs (sol(i) -sol(i-1)));
   if abs (sol(i) -sol(i-1)) <eps 
       notificationString='converged before maximum iterations is reached';
       disp ('converged')
       break;
   end
fprintf('\n');
time =toc;
end 
scatter(sol(i),f(i),'filled')