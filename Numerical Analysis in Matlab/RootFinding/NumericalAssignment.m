function varargout = NumericalAssignment(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @NumericalAssignment_OpeningFcn, ...
    'gui_OutputFcn',  @NumericalAssignment_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before NumericalAssignment is made visible.
function NumericalAssignment_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;


% Update handles structure
guidata(hObject, handles);



% --- Outputs from this function are returned to the command line.
function varargout = NumericalAssignment_OutputFcn(hObject, eventdata, handles)

varargout{1} = handles.output;

% --------------------------------------------------------------------------------INTRO----------------------------------------FirstButton------------------------------------------
function rootsFinding_Callback(hObject, eventdata, handles)
disp('llllllllll');
set(handles.rootsFinding, 'Visible', 'Off');
set(handles.interpolation, 'Visible', 'Off');
set(handles.timeTxt,'Visible','on');

set(handles.GettingStarted, 'Title', 'Roots Finding');
set(handles.rootsFindingPanel, 'Visible', 'on');
set(handles.newtonPanel,'Visible','on');
set(handles.notificationText,'Visible','on');
abc = {'precision',''; 'Max. # iterations','50';'epsilon','0.00001'};
set(handles.dataTable,'Data',abc);


% --- Executes on button press in interpolation.
function interpolation_Callback(hObject, eventdata, handles)
set(handles.rootsFinding, 'Visible', 'Off');
set(handles.interpolation, 'Visible', 'Off');

% --------------------------------------------------------------------------------------------------------------------------------choosingMethodsButtoms-------------------------------
% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

function bisection_Callback(hObject, eventdata, handles)  %-----------------------------------  %bisection
set(handles.IterationsTable,'Data',[]);
set(handles.falsePosition,'Value',0);
set(handles.newton,'Value',0);
set(handles.fixedPoint,'Value',0);
set(handles.secant,'Value',0);
set(handles.biergeVieta,'Value',0);
abc = {'precision',''; 'Max. # iterations','50';'epsilon','0.00001';'lower bounds','0';'Upper bounds','1'};
set(handles.dataTable,'Data',abc);
set(handles.IterationsTable,'Visible','on');
xy = {'Xlower','f(Xlower)' ,'Xupper' ,'f(Xupper)' ,'Xr','f(Xr)','Error'};
set(handles.IterationsTable,'ColumnName',xy);
set(handles.gOfx,'Visible','off');
set(handles.g1,'Visible','off');
set(handles.g2,'Visible','off');


function falsePosition_Callback(hObject, eventdata, handles)  %-----------------------------------  %FalsePosition
set(handles.IterationsTable,'Data',[]);
set(handles.bisection,'Value',0);
set(handles.newton,'Value',0);
set(handles.fixedPoint,'Value',0);
set(handles.secant,'Value',0);
set(handles.biergeVieta,'Value',0);
abc = {'precision',''; 'Max. # iterations','50';'epsilon','0.00001';'lower bounds','0';'Upper bounds','1'};
set(handles.dataTable,'Data',abc);
set(handles.IterationsTable,'Visible','on');
xy = {'Xlower','f(Xlower)' ,'Xupper' ,'f(Xupper)' ,'Xr','f(Xr)','Error'};
set(handles.IterationsTable,'ColumnName',xy);
set(handles.gOfx,'Visible','off');
set(handles.g1,'Visible','off');
set(handles.g2,'Visible','off');


function fixedPoint_Callback(hObject, eventdata, handles) %-----------------------------------  %fixed Point
set(handles.IterationsTable,'Data',[]);
set(handles.falsePosition,'Value',0);
set(handles.newton,'Value',0);
set(handles.bisection,'Value',0);
set(handles.secant,'Value',0);
set(handles.biergeVieta,'Value',0);
ab = {'precision',''; 'Max. # iterations','50';'epsilon','0.00001';'initial guess','0'};
set(handles.dataTable,'Data',ab);
set(handles.IterationsTable,'Visible','on');
xy = {'X','X new' ,'Error'};
set(handles.IterationsTable,'ColumnName',xy);


function newton_Callback(hObject, eventdata, handles)  %-----------------------------------  %Newton
set(handles.IterationsTable,'Data',[]);
set(handles.falsePosition,'Value',0);
set(handles.bisection,'Value',0);
set(handles.fixedPoint,'Value',0);
set(handles.secant,'Value',0);
set(handles.biergeVieta,'Value',0);
ab = {'precision',''; 'Max. # iterations','50';'epsilon','0.00001';'initial guess','0'};
set(handles.dataTable,'Data',ab);
f = get(handles.equation,'String');
set(handles.IterationsTable,'Visible','on');
xy = {'X','f(X)' ,'f ''(x)' ,'Error'};
set(handles.IterationsTable,'ColumnName',xy);
set(handles.gOfx,'Visible','off');
set(handles.g1,'Visible','off');
set(handles.g2,'Visible','off');

function secant_Callback(hObject, eventdata, handles)  %-----------------------------------  %Secant
set(handles.IterationsTable,'Data',[]);
set(handles.falsePosition,'Value',0);
set(handles.newton,'Value',0);
set(handles.fixedPoint,'Value',0);
set(handles.bisection,'Value',0);
set(handles.biergeVieta,'Value',0);
abc2 = {'precision',''; 'Max. # iterations','50';'epsilon','0.00001';'x(0)','0';'x(1)','1'};
set(handles.dataTable,'Data',abc2);
set(handles.IterationsTable,'Visible','on');
xy = {'Xi-1','f(Xi-1)' ,'Xi','f(Xi)' ,'Xi+1','Error'};
set(handles.IterationsTable,'ColumnName',xy);
set(handles.gOfx,'Visible','off');
set(handles.g1,'Visible','off');
set(handles.g2,'Visible','off');


function biergeVieta_Callback(hObject, eventdata, handles)  %-----------------------------------  %bierge Vieta
set(handles.IterationsTable,'Data',[]);
set(handles.falsePosition,'Value',0);
set(handles.newton,'Value',0);
set(handles.fixedPoint,'Value',0);
set(handles.secant,'Value',0);
set(handles.bisection,'Value',0);
abc2 = {'precision',''; 'Max. # iterations','50';'epsilon','0.00001';'intial value','0'};
set(handles.dataTable,'Data',abc2);
set(handles.IterationsTable,'Visible','on');
xy = {'iteration number','a','b','c','Xi','Xi+1' ,'Error'};
set(handles.IterationsTable,'ColumnName',xy);
set(handles.gOfx,'Visible','off');
set(handles.g1,'Visible','off');
set(handles.g2,'Visible','off');

% --- Executes on button press in default.
function default_Callback(hObject, eventdata, handles)  %-----------------------------------  % Default
set(handles.IterationsTable,'Data',[]);
set(handles.falsePosition,'Value',0);
set(handles.newton,'Value',0);
set(handles.fixedPoint,'Value',0);
set(handles.secant,'Value',0);
set(handles.biergeVieta,'Value',0);
 msgbox('default method is Bisection Method','warning');
 abc = {'precision',''; 'Max. # iterations','50';'epsilon','0.00001';'lower bounds','0';'Upper bounds','1'};
set(handles.dataTable,'Data',abc);
set(handles.IterationsTable,'Visible','on');
xy = {'Xlower','f(Xlower)' ,'Xupper' ,'f(Xupper)' ,'Xr','f(Xr)','Error'};
set(handles.IterationsTable,'ColumnName',xy);
set(handles.gOfx,'Visible','off');
set(handles.g1,'Visible','off');
set(handles.g2,'Visible','off');
% ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



%---------------------------------------------------------------------------------------Solving---------------------------------------Solving--------------------------------------------------
%------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function solve_Callback(hObject, eventdata, handles)

    func = get(handles.equation,'String');
    disp('------');
    disp(isstr(func));
    disp('-----');
    if strcmp(func,'equation') || isempty(func)
            msgbox('No equation entered','warning');
        return;
    end
    set(handles.showEquation,'String',func);
     y= get(handles.dataTable,'Data');
     precision=y{1,2};
     maxIterations=y{2,2};
     epsilon=y{3,2};
     maxIterationsINT=str2double(maxIterations);
     epsilonDouble=str2double(epsilon);
    
     
if get(handles.bisection,'Value')==1 %---------------------------bisection--------------*****************************
    Xl=y{4,2};
    Xu=y{5,2};
    XlDouble=str2double(Xl);
    XuDouble=str2double(Xu);
    
   [validEntry,notificationString,itTable,time] =bisectionMethod(func,XlDouble,XuDouble,epsilonDouble,maxIterationsINT);
if validEntry==1
    disp(time);
       disp(notificationString);
          disp(itTable);
      set(handles.timeTxt,'String',time);
    set(handles.notificationText,'String',notificationString);
   set(handles.IterationsTable,'Data',itTable);
else
    msgbox(notificationString,'warning');
end
  

elseif   get(handles.falsePosition,'Value')==1   %------------------------falsePosition----**************************
    Xl=y{4,2};
    Xu=y{5,2};
    XlDouble=str2double(Xl);
    XuDouble=str2double(Xu);
    [validEntry,notificationString,itTable,time]=false_position(func,XlDouble,XuDouble,epsilonDouble,maxIterationsINT);
    if validEntry==1
        set(handles.timeTxt,'String',time);
        set(handles.IterationsTable,'Data',itTable);
        set(handles.notificationText,'String',notificationString);
     else
           msgbox(notificationString,'warning');
     end
 
elseif get(handles.fixedPoint,'Value')==1   %------------------------FixedPoint--------------***************************
    intialGuess=y{4,2};
    intialGuessDouble=str2double(intialGuess);

      [X,Y,validEntry,notificationString,gOfX,itTable,time]= FixedPt(func,intialGuessDouble,epsilonDouble,maxIterationsINT);
     if validEntry==1
set(handles.g2,'String',gOfX);
set(handles.gOfx,'Visible','on');
set(handles.g1,'Visible','on');
set(handles.g2,'Visible','on');
    set(handles.timeTxt,'String',time);
        set(handles.IterationsTable,'Data',itTable);
        set(handles.notificationText,'String',notificationString);
     else
           msgbox(notificationString,'warning');
     end
   
    
   
elseif get(handles.newton,'Value')==1   %------------------------Newton---------------*****************************
    intialGuess=y{4,2};
    intialGuessDouble=str2double(intialGuess);
    [validEntry,notiString , itTable,time] =NewtenMethod(func,intialGuessDouble,maxIterationsINT,epsilonDouble);
   
    if validEntry ==1
        set(handles.timeTxt,'String',time);
        set(handles.notificationText,'String',notiString);
        set(handles.IterationsTable,'Data',itTable);
    else
            msgbox(notiString,'warning');
    end
 
    
    
elseif get(handles.secant,'Value')==1 %------------------------Secant--------------------**********************************
    intialGuess=y{4,2};
    intialGuess2=y{5,2};
    intialGuessDouble=str2double(intialGuess);
    intialGuessDouble2=str2double(intialGuess2)
    [validEntry,notiString , itTable,time] =secant(func,intialGuessDouble,intialGuessDouble2,epsilonDouble,maxIterationsINT);
    if validEntry ==1
        set(handles.notificationText,'String',notiString);
        set(handles.IterationsTable,'Data',itTable);
        set(handles.timeTxt,'String',time);
    else
        msgbox(notiString,'warning');
    end
    
 
elseif get(handles.biergeVieta,'Value')==1    %------------------------BiergeVieta--------------****************************
    intialGuess=y{4,2};
    intialGuessDouble=str2double(intialGuess);
    [validEntry,notiString , itTable,time]=birgeVeita(func,intialGuessDouble,epsilonDouble,maxIterationsINT)
    if validEntry==1
         set(handles.notificationText,'String',notiString);
         set(handles.IterationsTable,'Data',itTable);
         set(handles.timeTxt,'String',time);
    else
           msgbox(notiString,'warning');
    end
    
elseif  get(handles.default,'Value')==1 %-----------------------Default--------------****************************
      Xl=y{4,2};
    Xu=y{5,2};
    XlDouble=str2double(Xl);
    XuDouble=str2double(Xu);
    
   [validEntry,notificationString,itTable,time] =bisectionMethod(func,XlDouble,XuDouble,epsilonDouble,maxIterationsINT);
if validEntry==1
      set(handles.notificationText,'String',notificationString);
         set(handles.IterationsTable,'Data',itTable);
         set(handles.timeTxt,'String',time);
else
    msgbox(notificationString,'warning');
end
 else
            msgbox('please choose way of solving ','warning');
end


% ---------------------------------------------------------enterEquation----------------VS-----------------Files---------------------------------------------------------------
function enterEquation_Callback(hObject, eventdata, handles)
set(handles. fromFile,'Value',0);
set(handles.chooseFile,'Visible','off');
set(handles.equation,'String','');
set(handles.equation,'Enable','on');


function fromFile_Callback(hObject, eventdata, handles)
set(handles.enterEquation,'Value',0);
set(handles.chooseFile,'Visible','on');
set(handles.equation,'String','');
set(handles.equation,'Enable','inactive');


function chooseFile_Callback(hObject, eventdata, handles)
[FileName,PathName] = uigetfile('*.txt','Select input file');
fullPath=strcat(PathName,FileName);
line =fileread(fullPath);
set(handles.equation,'Enable','on');
set(handles.equation,'String',line);




% --- Executes when selected cell(s) is changed in dataTable.
function dataTable_CellSelectionCallback(hObject, eventdata, handles)


% --- Executes when entered data in editable cell(s) in IterationsTable.
function IterationsTable_CellEditCallback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function equation_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function equation_Callback(hObject, eventdata, handles)

function file_Callback(hObject, eventdata, handles)


function file_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
