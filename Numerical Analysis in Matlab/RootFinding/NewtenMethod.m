function [validEntry,notificationString,tab, time]=NewtenMethod(func,initial,maxit,eps)  % latest version
time=0;
tic
disp('NEWTEN METHOD');
notificationString='no errors';
validEntry=1;
tab =cell(maxit,5);
syms x
  fig=figure;
A = sym(func);
b = matlabFunction(A);
dfunc = diff(b(x));
x = -10:.1:10;
plot(x,b(x))
hold on ;
sol(1)=initial
f(1)=feval(inline(func),sol(1));
dfdx(1)=feval(inline(dfunc),sol(1));
tab(1,1)=num2cell(initial);
 tab(1,2)=num2cell(f(1));
 tab(1,3)=num2cell(dfdx(1)); 
 tab(1,4)=num2cell(initial); 
disp (' Step         x                        f                        df/dx');
for i=2 :maxit
   sol(i)= sol(i-1)-f(i-1)/dfdx(i-1);
   f(i)=feval(inline(func),sol(i));
   dfdx(i)=feval(inline(dfunc),sol(i));
   tab(i,1)=num2cell(sol(i));
   tab(i,2)=num2cell(f(i));
   tab(i,3)=num2cell(dfdx(i));
   tab(i,4)=num2cell(abs (sol(i) -sol(i-1)));
   out = [i-1 ;sol(i-1) ; f(i-1) ; dfdx(i-1)];
       %-*------------------------------------------
    tang=(x-sol(i-1))*dfdx(i-1)+f(i-1);
    plot(x,tang, 'Color', rand(1,3))
    scatter(sol(i-1),f(i-1))
    hold all
    %-*------------------------------------------
   fprintf ('%5.0f     %20.14f     %21.15f     %21.15f\n',out);
   fprintf ('ERROR = %21.15f\n',abs (sol(i) -sol(i-1)));
   if abs (sol(i) -sol(i-1)) <eps
       notificationString='it reached desired epsilon before reaching Maximum iterations ';
       disp ('converged')
       break;
   end
time=toc;
end  
scatter(sol(i),f(i),'filled')


