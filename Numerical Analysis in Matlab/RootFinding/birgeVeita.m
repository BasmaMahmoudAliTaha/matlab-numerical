function  [validEntry,notiString , itTable,time]=birgeVeita(func,initial,eps,maxit)  % latest version
time=0;
tic
validEntry=1;
notiString='no errors;';
  fig=figure;
A = sym(func);
b = matlabFunction(A);
x = -10:.1:10;
plot(x,b(x))
hold on ;
co=sym2poly(sym(func))
b=0 ;
sol=0;
c=0;
power=length(co)
itTable=cell(maxit*(power+1),7);
sol(1)= initial
b(1)= co(1);
c(1)= co(1);
counter = 0
for k=2 :maxit 
     itTable(1+power*counter,1)=num2cell(k-1);
    for i=2 : power
        b(i)=b(i-1)*sol(k-1)+ co(i)
    end
    for i=2 :power-1
        c(i)=c(i-1)*sol(k-1)+b(i)
    end
    for i = 1+power*counter :power*(counter+1)
        itTable(i,2)=num2cell(co(i-power*counter))
        itTable(i,3)=num2cell(b(i-power*counter));
        if i <= power*(counter+1)-1
        itTable(i,4)=num2cell(c(i-power*counter))
        end
    end
    sol(k)= sol(k-1)-b(power)/c(power-1)
    itTable(1+power*counter,5)=num2cell(sol(k-1));
    itTable(1+power*counter,6)=num2cell(sol(k));
    itTable(1+power*counter,7)=num2cell(abs(sol(k)-sol(k-1)));
    if abs(sol(k)-sol(k-1))<=eps
        break ;
    end
            counter  = counter +1 ;

   time= toc
   
end
fi =feval(inline(func),sol(k));
scatter(sol(k),fi,'filled')

 